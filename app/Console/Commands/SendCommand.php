<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Services\OrderService;

class SendCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:send {orderId}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $order = OrderService::getOrder($this->argument('orderId'));
        
            if($order != null) OrderService::sendOrder($order);
            else $this->error('order not found');
        } catch(\Exception $e) {
            echo $e->getMessage();
        }
    }
}
