<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class SendData implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $data;
    
    private $repository;
    
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($data, $repository)
    {
        $this->data = $data;
        $this->repository = $repository;
        
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $host = env('SAP_HOST');
        $port = 8000;
        
        //if($this->repository->isAlreadySent($this->data->persistenceId)) return $this->logError("data already sent");
        
        try 
        {
            return $this->send($host, $port);
        } 
        catch(\Exception $e) 
        {
            return $this->logError($e->getMessage());
        }
    }
    
    private function send($host, $port) 
    {
        // No Timeout 
        set_time_limit(0);
        
        $socket = socket_create(AF_INET, SOCK_STREAM, 0); 
        
        if (!$socket) return $this->logError("Could not create socket");
        
        info('send to ' . $host . ':' . $port);
        
        $result = socket_connect($socket, $host, $port);
        
        if(!$result) return $this->logError("Could not connect to server $host:$port");
        
        $path = explode('\\', get_class($this->repository->getModel()));
        $name = snake_case(array_pop($path));
        $this->data->type = $name;
        
        $output = json_encode($this->data) . "\xD\xA";
        
        $sent = socket_write($socket, $output);
        
        if(!$sent) return $this->logError("Could not write output");
        
        $result = socket_read ($socket, 1024);
        
        if(!$result) return $this->logError("Could not read server response");
        
        $result = json_decode($result);
        
        socket_close($socket);
        
        if(isset($result->error)) return $this->logError($result->error);
        //else if(is_string($result)) return $this->logError($result);
        
        $msg = isset($result->message) ? $result->message : json_encode($result);
        return $this->logSuccess($msg);
    }
    
    private function logError($error) 
    {
        info('error:' . $error);
        
        return $this->repository->logError($this->data, $error);
    }
    
    private function logSuccess($msg) 
    {
        info('success:' . $msg);
        
        return $this->repository->logSuccess($this->data, $msg);
    }
}
