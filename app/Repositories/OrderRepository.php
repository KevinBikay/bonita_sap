<?php

namespace App\Repositories;

use Illuminate\Database\Eloquent\Model;
use App\Mail\SendOrderErrorMail;
use App\Models\Order;

use DB;
use Mail;

class OrderRepository extends Repository
{
    public function getModel(array $fields = array()) : Model 
    {
        return new Order($fields);
    }
}