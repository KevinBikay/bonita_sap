<?php

namespace App\Repositories;

use Illuminate\Database\Eloquent\Model;
use App\Models\Project;
use DB;

class ProjectRepository extends Repository
{
    public function getModel(array $fields = array()) : Model 
    {
        return new Project($fields);
    }
}