<?php

namespace App\Repositories;

use App\Mail\SendOrderErrorMail;
use Illuminate\Database\Eloquent\Model;

use DB;
use Mail;

abstract class Repository 
{
    public abstract function getModel(array $fields = array()) : Model;
    
    public function create(array $fields) 
    {
        $model = $this->getModel($fields);
        
        $model->save();
        
        return $model;
    }
    
    public function isAlreadySent($persistenceId) 
    {
        $model = $this->getModel();
        
        $data = DB::table($model->getTable())
                ->whereJsonContains('fields->persistenceId', $persistenceId)
                ->where('is_success', true)
                ->get();

        return count($data) > 0;
    }
    
    public function logError($data, $error) 
    {
        $fields = ['fields' => $data, 'is_success' => false, 'send_info' => $error];
        
        $data = $this->create($fields);
        
        return $data;
    }
    
    public function logSuccess($data, $message) 
    {
        $fields = ['fields' => $data, 'is_success' => true, 'send_info' => $message];
        
        return $this->create($fields);
    }
}