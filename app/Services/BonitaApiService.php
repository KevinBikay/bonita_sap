<?php

namespace App\Services;

use GuzzleHttp\Client;

class BonitaApiService 
{
    private $client;
    
    public function __construct() 
    {
        $this->client = $this->makeClient();
        
        $this->connect(env('BONITA_USER', 'walter.bates'), env('BONITA_PASSWORD', 'bpm'));
    }
    
    public function connect($username, $passwd) 
    {
        $response = $this->client->post($this->getUri('/loginservice'), [
            'form_params' => [
                'username' => $username,
                'password' => $passwd,
                'redirect' => false
            ]
        ]);
    }
    
    public function getOrder($orderID) 
    {
        $order = json_decode($this->client->get($this->getUri('API/bdm/businessData/ats.model.DistributionOrder/' . $orderID))->getBody());
        
        $items = json_decode($this->client->get($this->getUri('API/bdm/businessData/ats.model.DistributionOrder/' . $orderID . '/orderItems'))->getBody());
        
        $order->items = $items;
        
        return $order;
    }
    
    public function getProject($projectID)
    {
        $project = json_decode($this->client->get($this->getUri('API/bdm/businessData/ats.model.Project/' . $projectID))->getBody());
        
        return $this->withRelations($project);
    }
    
    public function findProjectByCode($projectCode) 
    {
        $projects = json_decode($this->client->get($this->getUri(
            'API/bdm/businessData/ats.model.Project?p=0&c=1&q=findByProjectCode&f=projectCode=' . $projectCode
        ))->getBody());
        
        $project = is_array($projects) && count($projects) > 0 ? $projects[0] : null;
        
        return $this->withRelations($project);
    }
    
    private function withRelations($model) 
    {
        if($model == null || !isset($model->links)) return $model;
        
        foreach($model->links as $link) 
        {
            $data = json_decode($this->client->get($this->getUri($link->href))->getBody());
            
            if($data == null) 
            {
                $model->{$link->rel} = null;
            }
            else if(is_object($data))
            {
                $model->{$link->rel} = $this->withRelations($data);
            }
            else if(is_array($data)) 
            {
                $model->{$link->rel} = $this->arrayWithRelations($data);
            }
        }
        
        return $model;
    }
    
    private function arrayWithRelations(array $data) 
    {
        foreach($data as &$elt) 
        {
            if(is_object($elt)) $elt = $this->withRelations($elt);
            else if(is_array($elt)) $elt = $this->arrayWithRelations($elt);
        }
        
        return $data;
    }
    
    private function getUri($uri) 
    {
        return '/bonita/' . ltrim($uri, '/');
    }
    
    private function makeClient() 
    {
        return new Client([
            // Base URI is used with relative requests
            'base_uri' => env('BONITA_URL'),
            // You can set any number of default request options.
            'timeout'  => 2.0,
            'cookies' => true,
            'verify' => false
        ]);
    }
}