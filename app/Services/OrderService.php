<?php

namespace App\Services;

use App\Jobs\SendData;
use App\Repositories\OrderRepository;
use App\Repositories\Repository;

class OrderService extends Service
{
    public function get($orderID)
    {
        return $this->srv->getOrder($orderID);
    }
    
    public function getRepository() : Repository
    {
        return new OrderRepository();
    }
}