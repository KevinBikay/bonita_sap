<?php

namespace App\Services;

use App\Jobs\SendData;
use App\Repositories\ProjectRepository;
use App\Repositories\Repository;

class ProjectService extends Service
{
    public function get($id)
    {
        return $this->srv->getProject($id);
    }
    
     public function getRepository() : Repository
     {
         return new ProjectRepository();
     }
}