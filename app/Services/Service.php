<?php

namespace App\Services;

use App\Jobs\SendData;
use App\Repositories\Repository;

abstract class Service 
{
    protected $srv;
    
    public abstract function get($id);
    
    public abstract function getRepository() : Repository;
    
    private $repository;
    
    public function __construct() 
    {
        $this->srv = new BonitaApiService();
        $this->repository = $this->getRepository();
    }
    
    public function send($id, $status)
    {
        if($status != 'approved') return ['ok' => false];
        
        $objectToSend = null;
        
        try {
             $objectToSend = $this->get($id);
        } catch(\Exception $e) {
            $this->repository->logError(['persistenceId' => $id], $e->getMessage());
            return ['ok' => false, 'error' => $e->getMessage()];
        }

        if($objectToSend != null) SendData::dispatch($objectToSend, $this->repository);
        
        return ['ok' => $objectToSend != null, 'data' => $objectToSend];
    }
}