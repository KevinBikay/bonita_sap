@extends('layouts.app')

@section('content')

    @if(Session::has('success_message'))
        <div class="alert alert-success">
            <span class="glyphicon glyphicon-ok"></span>
            {!! session('success_message') !!}

            <button type="button" class="close" data-dismiss="alert" aria-label="close">
                <span aria-hidden="true">&times;</span>
            </button>

        </div>
    @endif

    <div class="panel panel-default">

        <div class="panel-heading clearfix">

            <div class="pull-left">
                <h4 class="mt-5 mb-5">Orders</h4>
            </div>
            
        </div>
        
        @if(count($orders) == 0)
            <div class="panel-body text-center">
                <h4>No Orders Available.</h4>
            </div>
        @else
        <div class="panel-body panel-body-with-table">
            <div class="table-responsive">
                <table class="table table-striped ">
                    <thead>
                        <tr>
                            <th style="width:30%">Fields</th>
                            <th>Success?</th>
                            <th>Send Info</th>

                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($orders as $order)
                        <tr>
                            <td>{{ \Dumper::dump($order->fields) }}</td>
                            <td>{{ ($order->is_success) ? 'Yes' : 'No' }}</td>
                            <td>{{ $order->send_info }}</td>
                            <td>
                                <form method="POST" action="{{route('send-order')}}">
                                    <input type="hidden" name="status" value="approved" />
                                    <input type="hidden" name="salesOrderId" value="{{$order->fields['persistenceId']}}" />
                                    <input type="hidden" name="persistenceId" value="{{$order->fields['persistenceId']}}" />
                                    <input type="button" class="btnSend" value="Resend" />
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

            </div>
        </div>

        <div class="panel-footer">
            {!! $orders->render() !!}
        </div>
        
        @endif
    
    </div>
@endsection

@section('scripts')
    <script>
        $('.btnSend').click(function() {
            var form = $(this).closest('form');
            var data = form.serialize();
            
            $.ajax({
                type: form.attr('method'),
                url: form.attr('action'),
                data: data,
                dataType: "json",
                success: function(data) {
                    document.location.reload(true);
                },
                error: function() {
                    document.location.reload(true);
                }
           });
        });
    </script>
@endsection