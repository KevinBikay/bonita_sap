
<div class="form-group {{ $errors->has('fields') ? 'has-error' : '' }}">
    <label for="fields" class="col-md-2 control-label">Fields</label>
    <div class="col-md-10">
        <input class="form-control" name="fields" type="text" id="fields" value="{{ old('fields', optional($project)->fields) }}" minlength="1" placeholder="Enter fields here...">
        {!! $errors->first('fields', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('is_success') ? 'has-error' : '' }}">
    <label for="is_success" class="col-md-2 control-label">Is Success</label>
    <div class="col-md-10">
        <div class="checkbox">
            <label for="is_success_1">
            	<input id="is_success_1" class="" name="is_success" type="checkbox" value="1" {{ old('is_success', optional($project)->is_success) == '1' ? 'checked' : '' }}>
                Yes
            </label>
        </div>

        {!! $errors->first('is_success', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('send_info') ? 'has-error' : '' }}">
    <label for="send_info" class="col-md-2 control-label">Send Info</label>
    <div class="col-md-10">
        <input class="form-control" name="send_info" type="text" id="send_info" value="{{ old('send_info', optional($project)->send_info) }}" minlength="1" placeholder="Enter send info here...">
        {!! $errors->first('send_info', '<p class="help-block">:message</p>') !!}
    </div>
</div>

