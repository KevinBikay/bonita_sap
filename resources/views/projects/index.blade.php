@extends('layouts.app')

@section('content')

    @if(Session::has('success_message'))
        <div class="alert alert-success">
            <span class="glyphicon glyphicon-ok"></span>
            {!! session('success_message') !!}

            <button type="button" class="close" data-dismiss="alert" aria-label="close">
                <span aria-hidden="true">&times;</span>
            </button>

        </div>
    @endif

    <div class="panel panel-default">

        <div class="panel-heading clearfix">

            <div class="pull-left">
                <h4 class="mt-5 mb-5">Projects</h4>
            </div>

        </div>
        
        @if(count($projects) == 0)
            <div class="panel-body text-center">
                <h4>No Projects Available.</h4>
            </div>
        @else
        <div class="panel-body panel-body-with-table">
            <div class="table-responsive">

                <table class="table table-striped " style="border-collapse: collapse; table-layout: fixed;">  
                    <thead>
                        <tr>
                            <th width="50%">Fields</th>
                            <th width="15%">Is Success</th>
                            <th width="20%">Send Info</th>
                            <th width="15%"></th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($projects as $project)
                        <tr>
                            <td>{{ \Dumper::dump($project->fields) }}</td>
                            <td>{{ ($project->is_success) ? 'Yes' : 'No' }}</td>
                            <td>{{ $project->send_info }}</td>
                            <td>
                                <form method="POST" action="{{route('send-project')}}">
                                    <input type="hidden" name="status" value="approved" />
                                    <input type="hidden" name="salesOrderId" value="{{$project->fields['persistenceId']}}" />
                                    <input type="hidden" name="persistenceId" value="{{$project->fields['persistenceId']}}" />
                                    <input type="button" class="btnSend" value="Resend" />
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

            </div>
        </div>

        <div class="panel-footer">
            {!! $projects->render() !!}
        </div>
        
        @endif
    
    </div>
@endsection

@section('scripts')
    <script>
        $('.btnSend').click(function() {
            var form = $(this).closest('form');
            var data = form.serialize();
            
            $.ajax({
                type: form.attr('method'),
                url: form.attr('action'),
                data: data,
                dataType: "json",
                success: function(data) {
                    document.location.reload(true);
                },
                error: function() {
                    document.location.reload(true);
                }
           });
        });
    </script>
@endsection