<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes(['register' => false]);

//Route::group(['middleware' => 'auth'], function() {
Route::post('/send-order', 'OrdersController@sendOrder')->name('send-order');
Route::group([
    'prefix' => 'orders',
], function () {
    Route::get('/', 'OrdersController@index')
         ->name('orders.order.index');
    Route::get('/show/{order}','OrdersController@show')
         ->name('orders.order.show')->where('id', '[0-9]+');
});

Route::get('/home', 'HomeController@index')->name('home');
//));


Route::post('/send-project', 'ProjectsController@sendProject')->name('send-project');
Route::group([
    'prefix' => 'projects',
], function () {
    Route::get('/', 'ProjectsController@index')
         ->name('projects.project.index');
    Route::get('/create','ProjectsController@create')
         ->name('projects.project.create');
    Route::get('/show/{project}','ProjectsController@show')
         ->name('projects.project.show')->where('id', '[0-9]+');
    Route::get('/{project}/edit','ProjectsController@edit')
         ->name('projects.project.edit')->where('id', '[0-9]+');
    Route::post('/', 'ProjectsController@store')
         ->name('projects.project.store');
    Route::put('project/{project}', 'ProjectsController@update')
         ->name('projects.project.update')->where('id', '[0-9]+');
    Route::delete('/project/{project}','ProjectsController@destroy')
         ->name('projects.project.destroy')->where('id', '[0-9]+');
});
